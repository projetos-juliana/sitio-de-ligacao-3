package com.juliana.sitiodeligacao3;

import com.sun.jna.Library;
import com.sun.jna.Pointer;
import com.sun.jna.Structure;
import com.sun.jna.Structure.FieldOrder;

/**
 *
 * @author felth
 */
public interface SitioDeLigacaoJNA extends Library {

    @FieldOrder({"r1", "r2"})
    public static class pair_r extends Structure {

        public static class ByReference extends pair_r implements Structure.ByReference {
        }

        public static class ByValue extends pair_r implements Structure.ByValue {
        }

        public double r1;
        public double r2;
    }

    @FieldOrder({"theta", "pair"})
    public static class map_theta_r1r2 extends Structure {

        public static class ByReference extends map_theta_r1r2 implements Structure.ByReference {
        }

        public static class ByValue extends map_theta_r1r2 implements Structure.ByValue {
        }

        public Pointer theta;
        public pair_r.ByReference pair;
    }

    @FieldOrder({"type", "sv_constant", "fluorescence"})
    public static class experiment extends Structure {

        public static class ByReference extends experiment implements Structure.ByReference {
        }

        public static class ByValue extends experiment implements Structure.ByValue {
        };

        public String type;
        public double sv_constant;
        public double fluorescence;
    }

    @FieldOrder({"r1r2_ratio", "d", "r1r2_map", "bsa_experiment", "hsa_experiment"})
    public static class model extends Structure {

        public static class ByReference extends model implements Structure.ByReference {
        }

        public static class ByValue extends model implements Structure.ByValue {
        };

        public double r1r2_ratio;
        public double d;
        public map_theta_r1r2.ByReference r1r2_map;
        public experiment.ByReference bsa_experiment;
        public experiment.ByReference hsa_experiment;
    }

    //*******************************
    //Main Functions
    //*******************************
    public String get_version();

    public void deallocate_model(model.ByReference local_model);

    public model.ByReference allocate_model();

    public boolean has_valid_conditions(model.ByReference local_model);

    public double determine_r1r2_ratio(model.ByReference local_model);

    //Note: Internal C Library Function
    public pair_r determine_r2(model.ByReference local_model, double angle);

    //Note: Internal C Library Function
    public pair_r determine_r2_alternatively(model.ByReference local_model, double angle);

    public void build_results(model.ByReference local_model);

    public void build_results_alternatively(model.ByReference local_model);

    public void extrapolation(model.ByReference local_model);

    //*******************************
    //Basic Model Interface Functions
    //*******************************
    double get_r1r2_ratio(model.ByReference local_model);

    void set_r1r2_ratio(model.ByReference local_model, double r1r2_ratio_value);

    double get_d(model.ByReference local_model);

    void set_d(model.ByReference local_model, double d);

    map_theta_r1r2.ByReference get_r1r2_map(model.ByReference local_model);

    experiment.ByReference get_experiment(model.ByReference local_model, String type);

    void set_experiment(model.ByReference local_model, String type, double sv_constant, double fluorescence);

    //*******************************
    //Auxiliary Functions
    //*******************************
    //Pair Methods
    public pair_r.ByReference allocate_pair();

    public pair_r.ByReference allocate_n_pairs();

    public String print_pair(pair_r.ByReference local_pair_r);

    public void test_change_pair(pair_r.ByReference local_pair_r);

    public void deallocate_pair(pair_r.ByReference local_pair_r);

    //Map Theta R1R2 Methods
    public map_theta_r1r2.ByReference allocate_map_theta_r1r2();

    public String print_map_theta_r1r2_element(map_theta_r1r2.ByReference local_map_theta_r1r2, int pos);

    public String print_map_theta_r1r2(map_theta_r1r2.ByReference local_map_theta_r1r2);

    public void deallocate_map_theta_r1r2(map_theta_r1r2.ByReference local_map_theta_r1r2);

    //Experiment Methods
    public experiment.ByReference allocate_experiment(String type);

    public String print_experiment(experiment.ByReference local_experiment);

    public void test_change_experiment(experiment.ByReference local_experiment);

    public void deallocate_experiment(experiment.ByReference local_experiment);

    //Model Methods
    public String print_model(model.ByReference local_model);

    public String print_model_element(model.ByReference local_model, String element);

}
