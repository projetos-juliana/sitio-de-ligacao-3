package com.juliana.sitiodeligacao3;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;

/**
 *
 * @author Ju-eLeo
 */
public class Experimento {
    
    private final String tipo;
    private DoubleProperty constanteSV = new SimpleDoubleProperty();
    private DoubleProperty fluorecencia = new SimpleDoubleProperty();

    public Experimento(String tipo) {
        this.tipo = tipo;
    }

    public Experimento(String tipo, double constanteSV, double fluorecencia) {
        this(tipo);
        this.constanteSV.set(constanteSV);
        this.fluorecencia.set(fluorecencia);
    }

    public String getTipo() {
        return tipo;
    }

    public double getConstanteSV() {
        return constanteSV.get();
    }

    public double getFluorecencia() {
        return fluorecencia.get();
    }

    public void setConstanteSV(double constanteSV) {
        this.constanteSV.set(constanteSV);
    }

    public void setFluorecencia(double fluorecencia) {
        this.fluorecencia.set(fluorecencia);
    }
    
    public DoubleProperty constanteSVProperty() {
        return constanteSV;
    }
    
    public DoubleProperty fluorescenciaProperty() {
        return fluorecencia;
    }
    
}
