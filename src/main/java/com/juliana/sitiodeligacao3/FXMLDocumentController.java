package com.juliana.sitiodeligacao3;

import com.sun.jna.Native;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.net.URL;
import java.util.Comparator;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.SnapshotParameters;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.ScatterChart;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.HBox;
import javafx.util.Pair;
import javax.imageio.ImageIO;

/**
 *
 * @author Ju-eLeo
 */
public class FXMLDocumentController implements Initializable {

    @FXML
    private TextField tfConstSVB;
    @FXML
    private TextField tfConstSVH;
    @FXML
    private TextField tfFluorescenciaB;
    @FXML
    private TextField tfFluorescenciaH;
    @FXML
    private TextField tfResultados;
    @FXML
    private TextField tfConstD;
    @FXML
    private TextField tfSubstancia;
    @FXML
    private Button btExecutar;
    @FXML
    private Button btExecutarModelo;
    @FXML
    private Button btExecutarModelo2;
    @FXML
    private Button btExecutarExtrapolacao;
    @FXML
    private Button btSalvarTab;
    @FXML
    private Button btSalvarGraf;
    @FXML
    private RadioButton rbtDados;
    @FXML
    private RadioButton rbtTendencia;
    @FXML
    private TableView<Pair<Double, Pair<Double, Double>>> tbvR1R2;
    @FXML
    private TableColumn<Pair<Double, Pair<Double, Double>>, String> tbcThetaRadianos;
    @FXML
    private TableColumn<Pair<Double, Pair<Double, Double>>, String> tbcThetaGraus;
    @FXML
    private TableColumn<Pair<Double, Pair<Double, Double>>, String> tbcR1;
    @FXML
    private TableColumn<Pair<Double, Pair<Double, Double>>, String> tbcR2;
    @FXML
    private ScatterChart<Double, Double> sctChart;
    @FXML
    private LineChart<Double, Double> linChart;
    @FXML
    private Label lblJnaLibVersion;
    @FXML
    private Label lbCondicoesInvalidas;

    private final Pattern numericPattern = Pattern.compile("-?(\\d+(\\.)?(\\d+)?)?");
    private final ToggleGroup groupGraficos = new ToggleGroup();

    private final SitioDeLigacaoJNA sdlJNA = (SitioDeLigacaoJNA) Native.load("libsitio-de-ligacao-c-library", SitioDeLigacaoJNA.class);
    private final SitioDeLigacaoJNA.model.ByReference instanciaModelo;

    public FXMLDocumentController() {
//        testC();
        instanciaModelo = sdlJNA.allocate_model();
    }

    public SitioDeLigacaoJNA getSdlJNA() {
        return sdlJNA;
    }

    public SitioDeLigacaoJNA.model.ByReference getInstanciaModelo() {
        return instanciaModelo;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        lblJnaLibVersion.setText("C Library Version: " + sdlJNA.get_version());
        HBox parent = (HBox) btExecutarModelo2.getParent();
        parent.getChildren().remove(btExecutarModelo2);

        tbcThetaGraus.setCellValueFactory(value -> new SimpleStringProperty(value.getValue().getKey().intValue() + ""));
        tbcThetaRadianos.setCellValueFactory(value -> new SimpleStringProperty(Math.toRadians(value.getValue().getKey()) + ""));
        tbcR1.setCellValueFactory(value -> {
            Double r1 = value.getValue().getValue().getKey();
            Double r2 = value.getValue().getValue().getValue();

            return r1 != Double.NEGATIVE_INFINITY || r2 != Double.NEGATIVE_INFINITY ? new SimpleStringProperty(r1.toString()) : null;
        });
        tbcR2.setCellValueFactory(value -> {
            Double r1 = value.getValue().getValue().getKey();
            Double r2 = value.getValue().getValue().getValue();

            return r1 != Double.NEGATIVE_INFINITY || r2 != Double.NEGATIVE_INFINITY ? new SimpleStringProperty(r2.toString()) : null;
        });

        sctChart.titleProperty().bind(new SimpleStringProperty("Dados - ").concat(tfSubstancia.textProperty()));
        linChart.titleProperty().bind(new SimpleStringProperty("Tendência - ").concat(tfSubstancia.textProperty()));
        groupGraficos.getToggles().addAll(rbtDados, rbtTendencia);
        groupGraficos.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            @Override
            public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) {
                sctChart.setVisible(newValue.equals(rbtDados));
                linChart.setVisible(newValue.equals(rbtTendencia));
            }
        });
        rbtDados.setSelected(true);
        linChart.setCreateSymbols(false);

        setupInterfaceModelo();

        btExecutar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (sdlJNA.has_valid_conditions(instanciaModelo)) {
                    lbCondicoesInvalidas.setVisible(false);
                    sdlJNA.determine_r1r2_ratio(instanciaModelo);
                    tfResultados.setText(String.valueOf(instanciaModelo.r1r2_ratio));
                } else {
                    lbCondicoesInvalidas.setVisible(true);
                }
            }
        });
        btExecutarExtrapolacao.setOnAction((ActionEvent event) -> {
            if (sdlJNA.has_valid_conditions(instanciaModelo)) {
                lbCondicoesInvalidas.setVisible(false);
                sdlJNA.extrapolation(instanciaModelo);
                tfResultados.setText(String.valueOf(instanciaModelo.r1r2_ratio));
                showResults();
            } else {
                lbCondicoesInvalidas.setVisible(true);
                tbvR1R2.getItems().clear();
            }
        });
        btExecutarModelo.setOnAction((event) -> {
            sdlJNA.build_results(instanciaModelo);
            showResults();
        });
        btExecutarModelo2.setOnAction((event) -> {
            sdlJNA.build_results_alternatively(instanciaModelo);
            showResults();;
        });
        btSalvarTab.setOnAction((event) -> {
            try {
                writeExcel();
            } catch (Exception ex) {
                Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        btSalvarGraf.setOnAction((event) -> {
            try {
                saveCharts();
            } catch (IOException ex) {
                Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }

    public void produzirGraficos() {
        Series series1 = new Series();
        series1.setName("R1");
        Series series2 = new Series();
        series2.setName("R2");

        sctChart.getData().clear();
        linChart.getData().clear();
        tbvR1R2.getItems().stream().forEach((valor) -> {
            double angulo = valor.getKey();
            double r1 = valor.getValue().getKey().doubleValue();
            double r2 = valor.getValue().getValue().doubleValue();

            if (r1 != Double.NEGATIVE_INFINITY) {
                series1.getData().add(new XYChart.Data(angulo, r1));
            }
            if (r2 != Double.NEGATIVE_INFINITY) {
                series2.getData().add(new XYChart.Data(angulo, r2));
            }
        });
        sctChart.getData().addAll(series1, series2);
        linChart.getData().addAll(copySeries(series1), copySeries(series2));
    }

    public void saveCharts() throws IOException {
        sctChart.setVisible(true);
        linChart.setVisible(true);
        WritableImage snapshot = sctChart.snapshot(new SnapshotParameters(), null);
        ImageIO.write(SwingFXUtils.fromFXImage(snapshot, null), "png", new File("Gráfico Dados " + tfSubstancia.getText() + ".png"));
        snapshot = linChart.snapshot(new SnapshotParameters(), null);
        ImageIO.write(SwingFXUtils.fromFXImage(snapshot, null), "png", new File("Gráfico Tendência " + tfSubstancia.getText() + ".png"));
        if (groupGraficos.getSelectedToggle().equals(rbtDados)) {
            linChart.setVisible(false);
        } else {
            sctChart.setVisible(false);
        }
    }

    public static <S, T> XYChart.Series<S, T> copySeries(XYChart.Series<S, T> series) {
        return new XYChart.Series<>(series.getName(),
                series.getData().stream()
                        .map(data -> new XYChart.Data<>(data.getXValue(), data.getYValue()))
                        .collect(Collectors.toCollection(FXCollections::observableArrayList)));
    }

    public void writeExcel() throws Exception {
        Writer writer = null;
        try {
            File file = new File("Tabela de Resultados " + tfSubstancia.getText() + ".csv.");
            writer = new BufferedWriter(new FileWriter(file));

            String text = tfSubstancia.getText() + "\nÂngulo (graus);Ângulo (rad);R1;R2\n";
            for (Pair<Double, Pair<Double, Double>> dados : tbvR1R2.getItems().sorted(new Comparator<Pair<Double, Pair<Double, Double>>>() {
                @Override
                public int compare(Pair<Double, Pair<Double, Double>> o1, Pair<Double, Pair<Double, Double>> o2) {
                    return (int) ((o1.getKey() - o2.getKey()));
                }
            })) {
                double anguloGraus = dados.getKey();
                double anguloRadianos = Math.toRadians(dados.getKey());
                double r1 = dados.getValue().getKey();
                double r2 = dados.getValue().getValue();

                text += anguloGraus + ";" + anguloRadianos + ";"
                        + (dados.getValue().getKey() != Double.NEGATIVE_INFINITY ? dados.getValue().getKey() : "") + ";"
                        + (dados.getValue().getValue() != Double.NEGATIVE_INFINITY ? dados.getValue().getValue() : "") + "\n";
            }
            text = text.replace(".", ",");
            writer.write(text);
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (writer != null) {
                writer.flush();
                writer.close();
            }
        }
    }

    private void testC() {
        System.out.println("Início dos testes");
        SitioDeLigacaoJNA.pair_r.ByReference p = sdlJNA.allocate_pair();
        System.out.println("Par criado");
        String p_print;
        p_print = sdlJNA.print_pair(p);
        System.out.println("Teste de impressão");
        System.out.println(p_print);
        p.r1 = 2;
        p.r2 = 5;
        System.out.println("Valores alterados");
        p_print = sdlJNA.print_pair(p);
        System.out.println(p_print);
        sdlJNA.test_change_pair(p);
        p_print = sdlJNA.print_pair(p);
        System.out.println(p_print);
        sdlJNA.deallocate_pair(p);

//        SitioDeLigacaoJNA.map_theta_r1r2.ByReference m = sdlJNA.create_map_theta_r1r2();
//        g_m = m;
//        System.out.println("Mapa Criado");
//        String m_print = sdlJNA.print_map_theta_r1r2(m);
//        System.out.println("Teste de impressão");
//        System.out.println(m_print);
//        for (int i = 0; i < 180; i++) {
//            m.value.r1 = 1000;
//            m.value.r2 = 1000;
//            m.value.getPointer().setDouble(i * (2 * Native.getNativeSize(Double.TYPE)), i + 1);
//            m.value.getPointer().setDouble(i * (2 * Native.getNativeSize(Double.TYPE)) + Native.getNativeSize(Double.TYPE), i + 1);
////            System.out.println("theta[" + i + "] = " + m.theta.getDouble(i * Native.getNativeSize(Double.TYPE)));
//        }
//        System.out.println("Valores alterados");
//        System.out.println(sdlJNA.print_map_theta_r1r2(m));
        SitioDeLigacaoJNA.experiment.ByReference e = sdlJNA.allocate_experiment("BSA");
        System.out.println("Experimento criado.");
        System.out.println(sdlJNA.print_experiment(e));
        e.sv_constant = 100.54;
        e.fluorescence = 345.12984;
        System.out.println("Experimento alterado.");
        System.out.println(sdlJNA.print_experiment(e));
        sdlJNA.test_change_experiment(e);
        System.out.println(sdlJNA.print_experiment(e));
        sdlJNA.deallocate_experiment(e);
        System.out.println("Experimento destruído.");

        System.out.println("Antes de alocar");
        SitioDeLigacaoJNA.model.ByReference c_modelo = sdlJNA.allocate_model();
        System.out.println("Depois de alocar");
        System.out.println(sdlJNA.print_model(c_modelo));
        System.out.println(sdlJNA.print_experiment(c_modelo.bsa_experiment));
        System.out.println(sdlJNA.print_experiment(c_modelo.hsa_experiment));
        sdlJNA.deallocate_model(c_modelo);
        System.out.println("Depois de desalocar");
    }

    private void setupExperimento(TextField tfConstSV, TextField tfFluorescencia, SitioDeLigacaoJNA.experiment.ByReference experimento) {
        if (instanciaModelo != null) {
            tfConstSV.setText(String.valueOf(experimento.sv_constant));
            tfConstSV.textProperty().addListener(new ChangeListener<String>() {
                @Override
                public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                    if (newValue != null && numericPattern.matcher(newValue).matches() && newValue.indexOf("-") == newValue.lastIndexOf("-")) {
                        if (!newValue.isEmpty() && !newValue.contentEquals("-")) {
                            experimento.sv_constant = Double.valueOf(newValue);
                        }
                    } else {
                        tfConstSV.setText(oldValue);
                    }
//                    System.out.println(sdlJNA.print_experimento(experimento));
                }
            });
            tfConstSV.focusedProperty().addListener(new ChangeListener<Boolean>() {
                @Override
                public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                    if (!newValue && (tfConstSV.getText().isEmpty() || tfConstSV.getText().contentEquals("-"))) {
                        tfConstSV.setText(String.valueOf(experimento.sv_constant));
                    }
                }
            });

            tfFluorescencia.setText(String.valueOf(experimento.fluorescence));
            tfFluorescencia.textProperty().addListener(new ChangeListener<String>() {
                @Override
                public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                    if (newValue != null && numericPattern.matcher(newValue).matches() && newValue.indexOf("-") == newValue.lastIndexOf("-")) {
                        if (!newValue.isEmpty() && !newValue.contentEquals("-")) {
                            experimento.fluorescence = Double.valueOf(newValue);
                        }
                    } else {
                        tfFluorescencia.setText(oldValue);
                    }
//                    System.out.println(sdlJNA.print_experimento(experimento));
                }
            });
            tfFluorescencia.focusedProperty().addListener(new ChangeListener<Boolean>() {
                @Override
                public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                    if (!newValue && (tfFluorescencia.getText().isEmpty() || tfFluorescencia.getText().contentEquals("-"))) {
                        tfFluorescencia.setText(String.valueOf(experimento.fluorescence));
                    }
                }
            });
        }
    }

    private void setupInterfaceModelo() {
        tfConstD.setText(String.valueOf(instanciaModelo.d));
        tfConstD.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (newValue != null && numericPattern.matcher(newValue).matches() && newValue.indexOf("-") == newValue.lastIndexOf("-")) {
                    if (!newValue.isEmpty() && !newValue.contentEquals("-")) {
                        instanciaModelo.d = Double.valueOf(newValue);
                    }
                } else {
                    tfConstD.setText(oldValue);
                }
//                System.out.println(sdlJNA.print_modelo_element(instanciaModelo, "D"));
            }
        });
        tfConstD.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (!newValue && (tfConstD.getText().isEmpty() || tfConstD.getText().contentEquals("-"))) {
                    tfConstD.setText(String.valueOf(instanciaModelo.d));
                }
            }
        });

        tfResultados.setText(String.valueOf(instanciaModelo.r1r2_ratio));
        tfResultados.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (newValue != null && numericPattern.matcher(newValue).matches() && newValue.indexOf("-") == newValue.lastIndexOf("-")) {
                    if (!newValue.isEmpty() && !newValue.contentEquals("-")) {
                        instanciaModelo.r1r2_ratio = Double.valueOf(newValue);
                    }
                } else {
                    tfResultados.setText(oldValue);
                }
//                System.out.println(sdlJNA.print_modelo_element(instanciaModelo, "r1r2_ratio"));
            }
        });
        tfResultados.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (!newValue && (tfResultados.getText().isEmpty() || tfResultados.getText().contentEquals("-"))) {
                    tfResultados.setText(String.valueOf(instanciaModelo.r1r2_ratio));
                }
            }
        });

        setupExperimento(tfConstSVB, tfFluorescenciaB, instanciaModelo.bsa_experiment);
        setupExperimento(tfConstSVH, tfFluorescenciaH, instanciaModelo.hsa_experiment);
    }

    private void fillThetaR1R2Table() {
        for (int i = 0; i <= 180; i++) {
            double theta = instanciaModelo.r1r2_map.theta.getDouble(i * Native.getNativeSize(Double.TYPE));
            double r1;
            double r2;
            if (i == 0) {
                r1 = instanciaModelo.r1r2_map.pair.r1;
                r2 = instanciaModelo.r1r2_map.pair.r2;
            } else {
                r1 = instanciaModelo.r1r2_map.pair.getPointer().getDouble(i * (2 * Native.getNativeSize(Double.TYPE)));
                r2 = instanciaModelo.r1r2_map.pair.getPointer().getDouble(i * (2 * Native.getNativeSize(Double.TYPE)) + Native.getNativeSize(Double.TYPE));
            }
            tbvR1R2.getItems().add(new Pair<>(theta, new Pair<>(r1, r2)));
        }
    }

    private void showResults() {
        tbvR1R2.getItems().clear();
        fillThetaR1R2Table();
        produzirGraficos();
    }
}
