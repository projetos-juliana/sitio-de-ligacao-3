package com.juliana.sitiodeligacao3;

import java.util.ArrayList;
import java.util.List;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableMap;
import javafx.util.Pair;

/**
 *
 * @author Ju-eLeo
 */
public class Modelo {

    private DoubleProperty razaoR1R2 = new SimpleDoubleProperty();
    private DoubleProperty D = new SimpleDoubleProperty();
    private List<Experimento> experimentos = new ArrayList<>();
    private final ObservableMap<Double, Pair<Double, Double>> mapaR1R2 = FXCollections.observableHashMap();

    public Modelo() {
    }

    public List<Experimento> getExperimentos() {
        return experimentos;
    }

    public Experimento getExperimentoBovino() {
        return experimentos.stream()
                .filter(e -> e.getTipo().contentEquals("BSA"))
                .findAny()
                .orElse(null);
    }

    public Experimento getExperimentoHumano() {
        return experimentos.stream()
                .filter(e -> e.getTipo().contentEquals("HSA"))
                .findAny()
                .orElse(null);
    }

    public boolean possuiCondiçoesValidas() {
        double Kb = getExperimentoBovino().constanteSVProperty().get();
        double Kh = getExperimentoHumano().constanteSVProperty().get();
        double Fb = getExperimentoBovino().fluorescenciaProperty().get();
        double Fh = getExperimentoHumano().fluorescenciaProperty().get();

        return ((Kb * Fb) / (Kh * Fh)) - 1 >= 0;
    }

    public double getRazaoR1R2() {
        return razaoR1R2.get();
    }

    public DoubleProperty razaoR1R2Property() {
        return razaoR1R2;
    }

    public double getD() {
        return D.get();
    }

    public DoubleProperty DProperty() {
        return D;
    }

    public void setD(double D) {
        this.D.set(D);
    }

    public double determinarRazao() {
        double Kb = getExperimentoBovino().constanteSVProperty().get();
        double Kh = getExperimentoHumano().constanteSVProperty().get();
        double Fb = getExperimentoBovino().fluorescenciaProperty().get();
        double Fh = getExperimentoHumano().fluorescenciaProperty().get();

        razaoR1R2.set(Math.pow(((Kb * Fb) / (Kh * Fh)) - 1, 1.0 / 6.0));
        return razaoR1R2.get();
    }

    public void construirResultados() {
        for (double theta = 0; theta < Math.toRadians(180); theta += Math.toRadians(1)) {
            Pair<Double, Double> r1R2 = determinarR2(theta);
            mapaR1R2.put(theta, r1R2);
            //System.out.println("Theta: " + Math.toDegrees(theta) + " r1: " + mapaR1R2.get(theta).getKey() + " r2: " + mapaR1R2.get(theta).getValue());
        }
    }

    public void construirResultadosAlternativo() {
        for (double theta = 0; theta < Math.toRadians(180); theta += Math.toRadians(1)) {
            Pair<Double, Double> r1R2 = determinarR2Alternativo(theta);
            mapaR1R2.put(theta, r1R2);
            //System.out.println("Theta: " + Math.toDegrees(theta) + " r1: " + mapaR1R2.get(theta).getKey() + " r2: " + mapaR1R2.get(theta).getValue());
        }
    }

    public void extrapolacao() {
        determinarRazao();
        construirResultados();
    }

    private Pair<Double, Double> determinarR2(double angulo) {
        double r1 = 0, r2 = 0;
        if (angulo != 0.0) {
            double a = 1 - Math.pow(razaoR1R2.get(), 2);
            double b = 2 * D.get() * razaoR1R2.get() * Math.cos(angulo);
            double c = -Math.pow(D.get(), 2);
            double delta = Math.pow(b, 2) - (4 * a * c);

            if (delta >= 0) {
                r2 = (-(b) + Math.sqrt(delta)) / (2 * a);

                if (r2 < 0) {
                    r2 = (-(b) - Math.sqrt(delta)) / (2 * a);
                }
            }
        } else {
            r2 = D.get() / (1 + razaoR1R2.get());
        }
        r1 = razaoR1R2.get() * r2;

        return new Pair<>(r1 != 0 ? r1 : null, r2 != 0 ? r2 : null);
    }

    private Pair<Double, Double> determinarR2Alternativo(double angulo) {
        double r1 = 0, r2 = 0;
        double a = Math.pow(razaoR1R2.get(), 2) + 1 - (2 * razaoR1R2.get() * Math.cos(angulo));
        double b = 0.0;
        double c = -Math.pow(D.get(), 2);
        double delta = Math.pow(b, 2) - (4 * a * c);

        if (delta >= 0) {
            r2 = (-(b) + Math.sqrt(delta)) / (2 * a);

            if (r2 < 0) {
                r2 = (-(b) - Math.sqrt(delta)) / (2 * a);
            }
        }
        r1 = razaoR1R2.get() * r2;

        return new Pair<>(r1 != 0 ? r1 : null, r2 != 0 ? r2 : null);
    }

    public ObservableMap<Double, Pair<Double, Double>> getMapaR1R2() {
        return mapaR1R2;
    }
}
