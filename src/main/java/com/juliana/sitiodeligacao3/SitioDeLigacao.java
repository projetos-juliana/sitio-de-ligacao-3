package com.juliana.sitiodeligacao3;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 *
 * @author Ju-eLeo
 */
public class SitioDeLigacao extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/FXMLDocument.fxml"));
        Parent root = loader.load();
        final FXMLDocumentController controller = (FXMLDocumentController) loader.getController();

        Scene scene = new Scene(root);

        stage.setScene(scene);
        stage.setTitle("Sítio de Ligação");
        stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                controller.getSdlJNA().deallocate_model(controller.getInstanciaModelo());
            }
        });
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
